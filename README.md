# blue-test-article-api

## Installation on mac...
### 1. Make sure GoLang is installed
- Download Go from https://golang.org/dl/
- Follow the installer.
- ‘go version’ to confirm installation.
- **NOTE:** I am using go version go1.10.3 darwin/amd64 https://dl.google.com/go/go1.10.3.darwin-amd64.pkg

### 2. Check this folder location is in the GO PATH
- go env GOPATH
- https://github.com/golang/go/wiki/SettingGOPATH

### 3. Packages installed
- go get "github.com/gorilla/handlers"
- go get "github.com/gorilla/mux"
- go get "github.com/mattn/go-sqlite3"

### 4. Run

- go build
- ./blue-test-article-api

### 5. Test
I haven't implemented automated tests. I've used postman for manual testing - you can use the collection here:
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/12b33b1730ef35c0e30f)

### Note! Incomplete State.

Please consider, this is my very first attempt at GoLang!
https://ffxblue.github.io/interview-tests/test/article-api/

- have not implemented date field properly (just string at the moment)
- have not implemented filtering by date for GET /tags/{tagName}/{date} (returns all articles with matching tagName regardless of date)
- have not implemented relatedTags functionality (returns empty array)
- I added an additional get all api endpoint (as it was quick to add and helpful in manual testing) (note: this endpoint does not return tags for articles)

## Assumptions

Made some assumptions for ease of implementation

- article has auto incrementing id
- title can be 50 chars, body can be 500
- made date a string (assumption 'formatted' date 10 chars) as rushing this
- have not implemented thread safety (one request at a time)
- there is no authorization / authentication on the endpoint
- articles list in 'tags' is a list of article ids that contain the tag and the date
- no need for data integrity across the tag data (tag-articles vs tag-relatedtags) so just used strings

## Description

### Design
- Tried to split out code base for extendability / testability
- Followed REST principles for the api design (eg 201 created with location returned)

The following areas I did not implement in this solution which I feel would be required for a production solution:

### 1. Testing
I would normally follow TDD practice when developing.
I tried to structure the code in a way that seemed testable, but I did not have time to learn the testing frameworks for GoLang as well.
I would normally include

- unit testing for each file included
- api contract testing (using a tool such as Pact https://github.com/pact-foundation/pact-go)

### 2. Commit hygiene
I would follow the teams branching and pull request / review strategy with regular commits. (Issue number / reference in the branch name usually)

### 3. Database
- ORM - I didn't include an ORM layer in this solution, but an ORM is quite often appropriate in a production solution
- Connection Management, Transaction Management, Thread Safety
-- I did the absolute bear minimum to get this working in an in mem db with assumption of single threaded use.  A production solution would require the above considerations.

### 4. Configuration / Deployment
- Configuration (for example db type and connection details) should normally be managed externally with the ability to inject depending on the deployment environment
- Containerisation - would provide Docker file and/or kubernetes config for containerised deployments
- I would expect a service like this to be deployed in a microservices environment where there were additional tools used for managing microservices interactions (e.g Istio)