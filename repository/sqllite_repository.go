package repository

import (
	"fmt"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"../article-api"
)

type Repository struct {
	DB *sql.DB
}

func NewTestInMemDatabase() article_api.ArticleRepository {
	db, err := sql.Open("sqlite3", ":memory:")
	panicErr(err)

	_, err = db.Exec(createArticlesTable)
	panicErr(err)

	_, err = db.Exec(createArticleTagsTable)
	panicErr(err)

	return &Repository{
		DB: db,
	}
}

func (r Repository) AddArticle(article article_api.Article) article_api.Article {

	stmt, err := r.DB.Prepare("INSERT INTO articles(title, body, article_date) values(?,?,?)")
	panicErr(err)

	res, err := stmt.Exec(article.Title, article.Body, article.Date)
	panicErr(err)

	id, err := res.LastInsertId()
	panicErr(err)

	article.ID = id
	r.AddTagsForArticle(id, article.Tags)
	fmt.Println(article)

	return article
}

func panicErr(err error) {
	if err != nil {
		log.Fatal(err)
		panic(err)
	}
}

func (r Repository) AddTagsForArticle(articleId int64, tags []string) {

	trashSQL, err := r.DB.Prepare("INSERT INTO articleTags(article, tagName) values(?,?)")
	panicErr(err)
	tx, err := r.DB.Begin()
	if err != nil {
		fmt.Println(err)
	}
	for _, tag := range tags {
		_, err = tx.Stmt(trashSQL).Exec(articleId, tag)
		if err != nil {
			break
		}
	}
	if err != nil {
		fmt.Println("doing rollback")
		tx.Rollback()
	} else {
		tx.Commit()
	}
}

func (r Repository) getTagsForArticle(article_id int64) []string {
	tags := []string{}

	rows, err := r.DB.Query("select tagName from articleTags where article = ?", article_id)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() {
		var tagName string
		err := rows.Scan(&tagName)
		if err != nil {
			log.Fatal(err)
		}
		tags = append(tags, tagName)
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}

	return tags
}


func (r Repository) getArticleIdsForTag(tagName string) []int64 {
	articleIds := []int64{}

	rows, err := r.DB.Query("select article from articleTags where tagName = ?", tagName)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() {
		var article int64
		err := rows.Scan(&article)
		if err != nil {
			log.Fatal(err)
		}
		articleIds = append(articleIds, article)
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}

	return articleIds
}

func (r Repository) GetArticleById(articleId int) article_api.Article {
	result := article_api.Article{}
	rows, err := r.DB.Query("select * from articles where _id = $1", articleId)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() {
		result = getArticleFromRow(rows)
	}
	err = rows.Err()
	panicErr(err)
	result.Tags = r.getTagsForArticle(result.ID)
	return result
}

func getArticleFromRow(rows *sql.Rows) article_api.Article {
	var id int64
	var title string
	var body string
	var article_date string
	err := rows.Scan(&id, &title, &body, &article_date)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(id, title, body, article_date)
	return article_api.Article{id, title, body, article_date, []string{}}
}

func (r Repository) GetTagByDate(tagName string, date string) article_api.Tag {
	relatedTags := []string{"not implemented"}
	articleIds := r.getArticleIdsForTag(tagName)
	return article_api.Tag{tagName, len(articleIds), articleIds, relatedTags}
}

// Additional function added here.
func (r Repository) GetArticles() article_api.Articles {
	results := article_api.Articles{}
	rows, err := r.DB.Query("select * from articles")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() {
		results = append(results, getArticleFromRow(rows))
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}

	return results
}

const createArticlesTable = `
CREATE TABLE IF NOT EXISTS articles
(
    _id INTEGER PRIMARY KEY,
    title VARCHAR(50),
    body VARCHAR(500),
    article_date VARCHAR(10)
)`

const createArticleTagsTable = `
CREATE TABLE IF NOT EXISTS articleTags
(
    _id INTEGER PRIMARY KEY,
    tagName VARCHAR(50),
    article INTEGER,
    FOREIGN KEY (article) REFERENCES articles(_id)
)`