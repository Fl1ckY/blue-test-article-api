package main

import (
	"log"
	"./article-api"
	sqliteRepo "./repository"
	"github.com/gorilla/mux"
	"github.com/gorilla/handlers"
	"net/http"
)

type App struct {
	Router *mux.Router
}

func (a *App) Initialize() {
	//TODO this would be managed better for different environments using some externalised config to decide?
	repository := sqliteRepo.NewTestInMemDatabase()
	a.Router = article_api.NewRouter(repository) // create routes
}

func (a *App) Run(addr string) {

	// Allow cross origin requests
	allowedOrigins := handlers.AllowedOrigins([]string{"*"})
	// Allow Get and Post request
	allowedMethods := handlers.AllowedMethods([]string{"GET", "POST"})

	// Launch server with CORS validations
	log.Fatal(http.ListenAndServe(addr, handlers.CORS(allowedOrigins, allowedMethods)(a.Router)))
}
