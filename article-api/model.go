package article_api

type Article struct {
	ID    int64    `bson:"_id"`
	Title string   `json:"title"`
	Body  string   `json:"body"`
	Date  string   `json:"date"`
	Tags  []string `json:"tags"`
}

type Tag struct {
	Tag          string   `json:"tag"`
	ArticleCount int      `json:"count"`
	Articles     []int64  `json:"articles"`
	RelatedTags  []string `json:"related_tags"`
}

type Articles []Article
