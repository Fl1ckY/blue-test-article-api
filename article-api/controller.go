package article_api

import (
	"net/http"
	"io/ioutil"
	"io"
	"log"
	"encoding/json"
	"strconv"
	"github.com/gorilla/mux"
)

type Controller struct {
	ArticleRepository ArticleRepository
}

func (c *Controller) setRepository(repository ArticleRepository) {
	c.ArticleRepository = repository
}

// AddArticle POST
func (c *Controller) AddArticle(w http.ResponseWriter, r *http.Request) {
	var article Article
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576)) // read the body of the request

	log.Println(body)

	if err != nil {
		log.Fatalln("Error AddArticle", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := r.Body.Close(); err != nil {
		log.Fatalln("Error AddArticle", err)
	}

	if err := json.Unmarshal(body, &article); err != nil { // unmarshall body contents as a type Candidate
		w.WriteHeader(422) // unprocessable entity
		log.Println(err)
		if err := json.NewEncoder(w).Encode(err); err != nil {
			log.Fatalln("Error AddArticle unmarshalling data", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}

	log.Println(article)
	createdArticle := c.ArticleRepository.AddArticle(article)
	location := "articles/" + strconv.FormatInt(createdArticle.ID, 10)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Location", location)
	w.WriteHeader(http.StatusCreated)
	return
}

// GetArticle GET - Gets a single article by ID
func (c *Controller) GetArticle(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	log.Println(vars)

	id := vars["id"] // param id
	log.Println(id);

	articleId, err := strconv.Atoi(id);

	if err != nil {
		log.Fatalln("Error GetArticle", err)
	}

	article := c.ArticleRepository.GetArticleById(articleId)
	data, _ := json.Marshal(article)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	w.Write(data)
	return
}

// GetArticles GET
func (c *Controller) GetArticles(w http.ResponseWriter, r *http.Request) {
	articles := c.ArticleRepository.GetArticles()
	data, _ := json.Marshal(articles)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	w.Write(data)
	return
}

// GetTagByDate GET /
func (c *Controller) GetTagByDate(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	log.Println(vars)

	tagName := vars["tagName"] // param id
	log.Println(tagName);

	date := vars["date"] // param id
	log.Println(date);

	tag := c.ArticleRepository.GetTagByDate(tagName, date)
	data, _ := json.Marshal(tag)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	w.Write(data)

	return
}



