package article_api

type ArticleRepository interface {
	AddArticle(article Article) Article
	GetArticleById(articleId int) Article
	GetArticles() Articles
	GetTagByDate(tagName string, date string) Tag
}
