package article_api

import (
	"log"
	"net/http"
	"github.com/gorilla/mux"
)

var controller = &Controller{}

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}
type Routes []Route

// NewRouter configures a new router to the API
func NewRouter(repository ArticleRepository) *mux.Router {

	controller.setRepository(repository)
	router := mux.NewRouter().StrictSlash(true)
	routes := setupRoutes()
	for _, route := range routes {
		var handler http.Handler
		log.Println(route.Name)
		handler = route.HandlerFunc

		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)
	}
	return router
}

func setupRoutes() Routes {
	return Routes{
		Route{
			"AddArticle",
			"POST",
			"/articles",
			controller.AddArticle,
		},
		Route{
			"GetArticle",
			"GET",
			"/articles/{id}",
			controller.GetArticle,
		},
		Route{
			"GetTagByDate",
			"GET",
			"/tags/{tagName}/{date}",
			controller.GetTagByDate,
		},
		Route{
			"GetArticles",
			"GET",
			"/articles",
			controller.GetArticles,
		}}
}
