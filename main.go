package main

import (
	"os"
)

func main() {
	a := App{}
	a.Initialize()

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	a.Run(":" + port)
}
